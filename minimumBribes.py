class Node:

    def __init__(self, val):
        self.val = val

    def __str__(self):
        return str(self.val)

# Complete the minimumBribes function below.
def minimumBribes(q):
    counter = 0
    real_q = [i for i in range(1,len(q)+1)]
    dict_index_q = {q[i]:Node(i) for i in range(len(q))}
    dict_index_real_q = {real_q[i]:Node(i) for i in range(len(q))}
    for i in range(len(real_q)):
        if(q[i] != real_q[i]):
            #print(i,"[",dict_index_q[q[i]],"] : ",q[i],"[",dict_index_real_q[real_q[i]],"] : ",real_q[i],)
            delta = dict_index_real_q[q[i]].val - dict_index_real_q[real_q[i]].val
            #print(delta,"|",q[i],real_q[i],"|",dict_index_real_q[q[i]].val ,dict_index_real_q[real_q[i]].val)
            if(delta > 2):
                print("Too chaotic")
                return
            else:
                counter += delta
                if(delta == 1):
                    real_q[i], real_q[i+delta] = real_q[i+delta], real_q[i]
                    dict_index_real_q[real_q[i]], dict_index_real_q[real_q[i+delta]] = dict_index_real_q[real_q[i+delta]], dict_index_real_q[real_q[i]] 
                elif(delta == 2):
                    real_q[i+1], real_q[i+delta] = real_q[i+delta], real_q[i+1]
                    dict_index_real_q[real_q[i+1]], dict_index_real_q[real_q[i+delta]] = dict_index_real_q[real_q[i+delta]], dict_index_real_q[real_q[i+1]]
                    real_q[i], real_q[i+1] = real_q[i+1], real_q[i]
                    dict_index_real_q[real_q[i]], dict_index_real_q[real_q[i+1]] = dict_index_real_q[real_q[i+1]], dict_index_real_q[real_q[i]]
    print(counter) 





if __name__ == '__main__':
    q = [int(i) for i in "5 1 2 3 7 8 6 4".split(" ")]

    minimumBribes(q)