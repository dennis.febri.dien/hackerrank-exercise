def countTriplets(arr, r):
    dict_counter = dict()
    for i in arr:
        if( i in dict_counter ):
            dict_counter[i] += 1
        else:
            dict_counter[i] = 1
    keys = list(dict_counter.keys())
    counter = 0
    for i in range(len(keys)-2):
        if(keys[i+1] == keys[i]*r and keys[i+2] == keys[i+1] *r):
            counter += (dict_counter[keys[i]]*dict_counter[keys[i+1]]*dict_counter[keys[i+2]])
    if(r == 1):
        print(dict_counter)
        counter = (dict_counter[1]-4)*(dict_counter[1]-5)*(dict_counter[1]-6)
    return counter

if __name__ == '__main__':
    nr = input().rstrip().split()

    n = int(nr[0])

    r = int(nr[1])

    arr = list(map(int, input().rstrip().split()))

    ans = countTriplets(arr, r)

    print(ans)