#!/bin/python3

import math
import os
import random
import re
import sys

def add_data(param, store_dict):
    if(param in store_dict["counter"]):
        before_add = store_dict["counter"][param]
        after_add = store_dict["counter"][param] + 1
        store_dict["counter"][param] = after_add
        if(len(store_dict["hasFreq"][before_add]) > 0):
            store_dict["hasFreq"][before_add].pop()
        if(after_add in store_dict["hasFreq"]):
            store_dict["hasFreq"][after_add].append(True)
        else:
            store_dict["hasFreq"][after_add] = [True]

    else:
        store_dict["counter"][param] = 1
        hasFreq = store_dict["counter"][param]
        if(hasFreq in store_dict["hasFreq"]):
            store_dict["hasFreq"][hasFreq].append(True)
        else:
            store_dict["hasFreq"][hasFreq] = [True]



def delete_data(param, store_dict):
    if(param in store_dict["counter"]):
        before_min = store_dict["counter"][param]
        after_min = store_dict["counter"][param] - 1
        check = False
        if(after_min == -1):
            after_min = 0
            check = True
        store_dict["counter"][param] = after_min
        if(len(store_dict["hasFreq"][before_min]) > 0):
            store_dict["hasFreq"][before_min].pop()
        if(after_min in store_dict["hasFreq"]):
            if(not check):
                store_dict["hasFreq"][after_min].append(True)
        else:
            if(not check):
                store_dict["hasFreq"][after_min] = [True]

def count_data(param, store_dict, output):
    checker = None
    if(param in store_dict["hasFreq"]):
        checker = len(store_dict["hasFreq"][param]) > 0
    else:
        checker = False
    if(checker):
        output.append(1)
    else:
        output.append(0)


def takeCommand(comm, param, store_dict, output):
    if(comm == 1):
        add_data(param, store_dict)
    elif(comm == 2):
        delete_data(param, store_dict)
    elif(comm == 3):
        count_data(param, store_dict, output)
    else:
        pass

# Complete the freqQuery function below.
def freqQuery(queries):
    output = list()
    store_dict = {
        "counter" : dict(),
        "hasFreq":{0:[]}
    }
    for query in queries:
        [comm, param] = query
        takeCommand(comm, param, store_dict, output)
    return output


if __name__ == '__main__':

    q = int(input().strip())

    queries = []

    for _ in range(q):
        queries.append(list(map(int, input().rstrip().split())))

    ans = freqQuery(queries)

    print(ans)
