class DataStorage:


    # array of matrix, first index indicates for start
    # and last index indicates for stops
    def __init__(self, n):
        self.minHMat = [[0 for j in range(n+1)] for i in range(n+1)]
        self.maxAreaMat = [[0 for j in range(n+1)] for i in range(n+1)]

    def isExist(self, start, stop):
        if(self.maxAreaMat[start][stop] == 0):
            return False
        else:
            return True

def lrHelperIter(arr, storage, start, stop):
    maximum_area = 0
    for i in range(0,len(arr)):
        if(i % 100 == 0):
            print("current iter : ",i)
        for j in range(0, len(arr)-i):
            # get index arr[j:j+i+1]
            start = j
            stop = j+i+1
            arr_slice = arr[start:stop]
            if(len(arr_slice) == 1):
                storage.minHMat[start][stop] = arr_slice[0]
                storage.maxAreaMat[start][stop] = arr_slice[0]
            else:
                areaLeft = storage.maxAreaMat[start][stop-1]
                areaRight = storage.maxAreaMat[start+1][stop]
                minhleft = storage.minHMat[start][stop-1]
                minhright = storage.minHMat[start+1][stop]
                minh = minhleft if(minhleft <= minhright) else minhright
                area = minh * len(arr[start:stop])
                storage.minHMat[start][stop] = minh
                storage.maxAreaMat[start][stop] = area
            if(maximum_area < storage.maxAreaMat[start][stop]):
                maximum_area = storage.maxAreaMat[start][stop]
    return maximum_area


# Complete the largestRectangle function below.
def largestRectangle(h):
    store = DataStorage(len(h))
    return lrHelperIter(h, store, 0, len(h))

def lrHelper(arr, storage, start, stop):
    if(len(arr) == 1):
        storage.minHMat[start][stop] = arr[0]
        storage.maxAreaMat[start][stop] = arr[0]
        return arr[0]
    else:
        areaLeft = None
        areaRight = None
        if(storage.isExist(start, stop-1)):
            areaLeft = storage.maxAreaMat[start][stop-1]
        else:
            areaLeft = lrHelper(arr[0:len(arr)-1], storage, start, stop-1)
        if(storage.isExist(start+1, stop)):
            areaRight = storage.maxAreaMat[start+1][stop]
        else:
            areaRight = lrHelper(arr[1:len(arr)], storage, start+1, stop)
        minhleft = storage.minHMat[start][stop-1]
        minhright = storage.minHMat[start+1][stop]
        minh = minhleft if(minhleft <= minhright) else minhright
        area = minh*(len(arr))
        storage.minHMat[start][stop] = minh
        storage.maxAreaMat[start][stop] = area 
        return max([area, areaLeft, areaRight])



if __name__ == '__main__':
    s = open("LargestRectangle.txt")
    d = s.readline()
    data = s.readline()
    x = largestRectangle([int(i) for i in data.split(" ")])
    print(x)