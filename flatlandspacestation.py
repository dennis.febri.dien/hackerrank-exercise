#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the flatlandSpaceStations function below.
def flatlandSpaceStations(n, c):
    arr_spacestations = [False]*n
    arr_distance = [0]*n
    arr_rev_distance = [0]*n
    dict_index_c = {c[i]:i for i in range(len(c))}
    for i in c:
        arr_spacestations[i] = True
    print(arr_spacestations)
    counter = 0
    first_detect = False
    for i in range(n):
        (counter, first_detect) = check_arr_distance(arr_distance, arr_spacestations, i, counter, first_detect)
    counter = 0
    first_detect = False
    for i in range(1,n+1):
        (counter, first_detect) = check_arr_distance(arr_rev_distance, arr_spacestations, -i, counter, first_detect)
    print(arr_distance)
    print(arr_rev_distance)
    arr_res = [0]*n
    maximum = 0
    for i in range(n):
        if(arr_distance[i] <= arr_rev_distance[i]):
            arr_res[i] = arr_distance[i]
        else:
            arr_res[i] = arr_rev_distance[i]
        if(arr_res[i] > maximum):
            maximum = arr_res[i]
    print(arr_res)
    print(maximum)

def check_arr_distance(arr,arr_check,i, counter, first_detect):
    if(arr_check[i] == True):
        counter = 0
        first_detect = True
    else:
        if(first_detect):
            
            counter += 1
    arr[i] = counter
    return (counter, first_detect)

if __name__ == "__main__":
    flatlandSpaceStations(100,[0])