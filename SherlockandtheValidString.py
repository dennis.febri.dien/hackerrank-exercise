import math


# Complete the isValid function below.
def isValid(s):
    ALPHABET = 'abcdefghijklmnopqrstuvwxyz'
    TOTAL_ALPHABET = len(ALPHABET)
    ARR_COUNT_CHAR = [0]*TOTAL_ALPHABET
    DICT_TO_ARR_CHAR = {ALPHABET[i]:i for i in range(TOTAL_ALPHABET)}

    for i in s:
        ARR_COUNT_CHAR[DICT_TO_ARR_CHAR[i]] += 1

    set_count = set(ARR_COUNT_CHAR)
    list_count = list(set_count)
    dict_dict = {i:0 for i in list_count}

    if(len(list_count) > 3):
        return "NO1"
    elif(len(list_count) <= 2):
        return "YES2"
    else:
        for i in ARR_COUNT_CHAR:
            dict_dict[i] += 1
        isOne = False
        for i in list_count:
            if(dict_dict[i] == 1):
                isOne =True
        print(ARR_COUNT_CHAR)
        print(dict_dict)
        print(math.fabs(list_count[2]-list_count[1]))
        if(dict_dict[1] ):
            return "YES3"
        elif(isOne and math.fabs(list_count[2]-list_count[1]) <= 1 ):
            return "YES4"
        else:
            return "NO5"



if __name__ == '__main__':
    print(isValid("aaaabbcc"))