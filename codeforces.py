if __name__ == '__main__':

    n = int(input())
    s = [int(i) for i in input().split(" ")]
    even = 0
    odd = 0
    out = -1
    for i in range(n):
        if(s[i]%2 == 1):
            odd += 1
        else:
            even += 1
        if(even >= 1 and odd >= 1):
            if(i == 1):
                if(s[i+1]%2 != s[i]%2):
                    out = i+1
                else:
                    out = i
            else:
                out = i+1
            break
    print(out)
